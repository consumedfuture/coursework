class UsersController < ApplicationController
    before_filter :admin, :only => [:index, :new, :create, :show, :delete, :destroy, :update]
    #before_filter :save_login_state, :only => [:login, :login_attempt]
    def auth
    end
    
    def check_auth
        cur_user = User.where('username = ?', params[:username]).take
        if cur_user
            salt = cur_user.salt
            authorized_user = User.where('username = ? AND password = ?', params[:username],BCrypt::Engine.hash_secret(params[:password],salt)).take
        end
    if authorized_user
      session[:user_id] = authorized_user.id
      flash[:success] = "OK"
      if authorized_user.type_role==1
        redirect_to(:controller => 'users', :action => 'index')
      else
        redirect_to(:controller => 'patients', :action => 'index')
      end
    else
      flash[:danger] = "Неверный логин или пароль"
      render "auth"	
    end
end
    def logout
        session[:user_id] = nil
        redirect_to :action => 'auth'
    end
    
  def index
      @users = User.all
  end

  def new
      @user = User.new
  end

  def create
      @user = User.new(user_params)
      if @user.save
          redirect_to(:action => 'index')
      else
          render('new')
      end
  end
    
  def show
      @user = User.find(params[:id])
  end

  def edit
      @user = User.find(params[:id])
  end

  def update
      @user = User.find(params[:id])
      if @user.update_attributes(user_params)
          redirect_to(:action => 'show', :id => @user.id)
      else
          render('index')
      end
  end

  def delete
      @user = User.find(params[:id])
  end
    
  def destroy
      @user = User.find(params[:id]).destroy
      redirect_to(:action => 'index')
  end
    
private 
    def user_params
        params.require(:user).permit(:username, :password, :surname, :name, :patronymic, :organization_id, :type_role, :salt)
    end
end
