class PatientsController < ApplicationController
    require 'date'
	before_filter :o_first || :o_second, :only => [:index, :show]
    before_filter :o_first, :only => [:new, :create, :edit, :update, :new_comission, :create_comission]
    before_filter :o_second, :only => [:new_result, :create_result]

	#before_filter :o_second, :only => [:index]
	def index
        @patients = Patient.all
    	@current_user = User.find session[:user_id]
    		if @current_user.type_role==2
    			render 'index_1'
    		end
    		if @current_user.type_role==3
    			render 'index_2'
    		end
            if @current_user.type_role==1
                redirect_to(:controller => 'users', :action => 'index')
            end
	end

    def show
        @patient = Patient.find(params[:id]) 
        @current_user = User.find session[:user_id]
            if @current_user.type_role==2
                render 'show_1'
            end
            if @current_user.type_role==3
                render 'show_2'
            end
            if @current_user.type_role==1
                redirect_to(:controller => 'users', :action => 'index')
            end
    end

    def new
        @patient = Patient.new
    end

    def create 
        #params[:register] = Date.today
        @patient = Patient.new(register_params)
        if @patient.save 
            @patient.update_attribute(:register, Date.today)
            redirect_to :action => 'index'
        else
            render 'new'
        end
    end

    def edit
        @patient = Patient.find(params[:id])
    end

    def update 
        @patient = Patient.find(params[:id])
        if @patient.update_attributes(register_params)
            redirect_to(:action => 'show', :id => @patient.id)
        else
            render('index')
        end
    end

    def new_comission
        @patient = Patient.find(params[:id])
    end

    def create_comission
        @patient = Patient.find(params[:id])
        if @patient.update_attributes(comission)
            @patient.update_attribute(:date_report, Date.today)
            @digit = Digits.find(1)
            if @patient.numreport==0 
                @digit.update_attribute(:report, @digit.report+1)
                @patient.update_attribute(:numreport, @digit.report)
            end
            if ((@patient.numdirection==0)&&(@patient.report_id==1))
                @digit.update_attribute(:direction, @digit.direction+1)
                @patient.update_attribute(:numdirection, @digit.direction)
            end
            redirect_to :action => 'show'
        else
            render 'new_comission'
        end
    end

    def new_result
        @patient = Patient.find(params[:id])
    end

    def create_result
        @patient = Patient.find(params[:id])
        if @patient.update_attributes(result)
            @digit = Digits.find(1)
            if @patient.numresult==0 
                @digit.update_attribute(:report, @digit.report+1)
                @patient.update_attribute(:numreport, @digit.report)
            end
            redirect_to :action => 'show'
        else
            render 'new_result'
        end
    end

    def delete
        @patient = Patient.find(params[:id])
    end
    
    def destroy
        @patient = Patient.find(params[:id]).destroy
        redirect_to(:action => 'index')
    end

private
    def register_params
        params.require(:patient).permit(:surname, :name, :patronymic, :birthday, :phone, :snils, :diagnosis_id)
    end

    def comission
        params.require(:patient).permit(:report_id, :numreport, :organization_id, :numdirection)
    end

    def result
        params.require(:patient).permit(:proc_begin, :proc_end, :result_id)
    end
end
