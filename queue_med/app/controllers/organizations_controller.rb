class OrganizationsController < ApplicationController
    before_filter :admin, :only => [:index, :new, :create]
      def index
      @organizations = Organization.all
  end

  def new
      @organization = Organization.new
  end

  def create
      @organization = Organization.new(organization_params)
      if @organization.save
          redirect_to(:action => 'index')
      else
          render('new')
      end
  end
    
  def edit
      @organization = Organization.find(params[:id])
  end

  def update
      @organization = Organization.find(params[:id])
      if @organization.update_attributes(organization_params)
          redirect_to(:action => 'index')     
      end
  end

  def delete
      @organization = User.find(params[:id])
  end
    
  def destroy
      @organization = User.find(params[:id]).destroy
      redirect_to(:action => 'index')
  end
    
private 
    def organization_params
        params.require(:organization).permit(:name, :type_org)
    end
end
