class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
protected 
def authenticate_user
  if session[:user_id]
    @current_user = User.find session[:user_id] 
    return true	
  else
    redirect_to(:controller => 'users', :action => 'auth')
    flash[:danger] = 'Необходимо авторизоваться'
    return false
  end
end
protected
def admin
  if session[:user_id]
    @current_user = User.find session[:user_id]
    if @current_user.type_role==1
        return true 
    else 
      redirect_to(:controller => 'patients', :action => 'index')
      flash[:danger] = 'Недостаточно прав для просмотра списка пользователей / организаций'
      return false
    end
  else
    redirect_to(:controller => 'users', :action => 'auth')
    flash[:danger] = 'Необходимо авторизоваться'
    return false
  end
end

def o_first
  if session[:user_id]
    @current_user = User.find session[:user_id]
    if @current_user.type_role==2
        return true 
    else 
      case @current_user.type_role
      when 1 then redirect_to(:controller => 'users', :action => 'index')
      end
      flash[:danger] = 'Недостаточно прав 2'
      return false
    end
  else
    redirect_to(:controller => 'users', :action => 'auth')
    flash[:danger] = 'Необходимо авторизоваться'
    return false
  end
end

def o_second
  if session[:user_id]
    @current_user = User.find session[:user_id]
    if @current_user.type_role==3
        return true 
    else 
      case @current_user.type_role
      when 1 then redirect_to(:controller => 'users', :action => 'index')
      end
      flash[:danger] = 'Недостаточно прав 3'
      return false
    end
  else
    redirect_to(:controller => 'users', :action => 'auth')
    flash[:danger] = 'Необходимо авторизоваться'
    return false
  end
end
#def save_login_state
#  if session[:user_id]
#    redirect_to(:controller => 'users', :action => 'index')
#    return false
#  else
#    return true
#  end
#end
end
