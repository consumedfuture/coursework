class Patient < ActiveRecord::Base
	belongs_to :organization
	belongs_to :result
	belongs_to :report
	belongs_to :diagnosis
end
