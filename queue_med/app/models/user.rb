class User < ActiveRecord::Base
belongs_to :organization
    before_save :encrypt_password
    after_save :clear_password
def encrypt_password
    if self.password.present?
        simple_password = self.password
        self.salt = BCrypt::Engine.generate_salt
        self.password = BCrypt::Engine.hash_secret(simple_password,salt)
    end
end
def clear_password
   self.password=nil 
end
end
