# This file should contain all the record creation needed to seed the database with its default values. 
# The data canthen be loaded with the rake db:seed (or created alongside the db with db:setup). # # Examples: # 
#   cities =City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }]) # Mayor.create(name: 'Emanuel', city: cities.first) 

diagnoses = Diagnosis.create([{name: 'N97.0 Женское бесплодие, связанное с отсутствием овуляции'}, {name: 'N97.1 Женское бесплодие
трубного происхождения'}, {name: 'N97.2 Женское бесплодие маточного происхождения'}, {name: 'N97.3 Женское бесплодие
цервиакального происхождения'}, {name: 'N97.4 Женское бесплодие, связанное с мужским фактором'}, {name: 'N97.8 Другие формы женского бесплодия'}, {name: 'N97.9 Женское бесплодие неуточненное'}])

reportresults = Report.create([{name: 'Направить на проведение процедуры ЭКО в'},{name: 'Направить на консультацию по поводу оказания ВМП'},{name: 'Отказать в проведении процедуры ЭКО'}])

results = Result.create([{name: 'Беременность наступила'}, {name: 'Беременность не наступила'}])

digits = Digits.create({report: 1, direction: 1, result: 1})