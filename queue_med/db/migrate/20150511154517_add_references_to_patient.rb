class AddReferencesToPatient < ActiveRecord::Migration
  def change
  	remove_column :patients, :report_result, :integer
  	remove_reference :patients, :result
  	add_reference :patients, :result
  	add_reference :patients, :report
  end
end
