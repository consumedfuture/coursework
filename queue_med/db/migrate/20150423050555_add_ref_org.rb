class AddRefOrg < ActiveRecord::Migration
  def change
      remove_column :users, :organization, :string
      #add_column :users, :organization, :belongs_to
      add_reference :users, :organization
  end
end
