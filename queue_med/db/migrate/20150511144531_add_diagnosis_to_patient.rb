class AddDiagnosisToPatient < ActiveRecord::Migration
  def change
  	add_reference :patients, :diagnosis
  end
end
