class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
    	t.string :code
    	t.string :pin
    	t.string :surname
    	t.string :name
    	t.string :patronymic
    	t.date :birthday
    	t.string :phone
    	t.string :snils
    	t.date :register
    	t.date :report
    	t.integer :report_result, :default => 0
    	t.integer :numreport, :default => 0
    	t.integer :numdirection, :default => 0
    	t.references :organization 
    	t.date :proc_begin
    	t.date :proc_end
    	t.references :result
    	t.integer :numresult, :default => 0
      t.timestamps
    end
  end
end
