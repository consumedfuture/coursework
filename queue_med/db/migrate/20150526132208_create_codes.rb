class CreateCodes < ActiveRecord::Migration
  def change
    create_table :codes do |t|
    	t.integer :code
      t.timestamps
    end
  end
end
