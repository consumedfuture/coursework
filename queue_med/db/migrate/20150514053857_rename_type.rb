class RenameType < ActiveRecord::Migration
  def change
  	rename_column :organizations, :type, :type_org
  end
end
