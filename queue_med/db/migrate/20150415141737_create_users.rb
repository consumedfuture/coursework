class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
        t.string :username
        t.string :password
        t.string :surname
        t.string :name
        t.string :patronymic
        t.string :organization
        t.integer :type_role
      t.timestamps
    end
  end
end
