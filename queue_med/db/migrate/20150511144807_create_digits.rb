class CreateDigits < ActiveRecord::Migration
  def change
    create_table :digits do |t|
    	t.integer :report
    	t.integer :direction
    	t.integer :result
      t.timestamps
    end
  end
end
