# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150526132354) do

  create_table "diagnoses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "digits", force: true do |t|
    t.integer  "report"
    t.integer  "direction"
    t.integer  "result"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "code"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type_org"
  end

  create_table "patients", force: true do |t|
    t.string   "code"
    t.string   "pin"
    t.string   "surname"
    t.string   "name"
    t.string   "patronymic"
    t.date     "birthday"
    t.string   "phone"
    t.string   "snils"
    t.date     "register"
    t.integer  "numreport",       default: 0
    t.integer  "numdirection",    default: 0
    t.integer  "organization_id"
    t.date     "proc_begin"
    t.date     "proc_end"
    t.integer  "numresult"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "diagnosis_id"
    t.integer  "result_id"
    t.integer  "report_id"
    t.date     "date_report"
  end

  create_table "reports", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "results", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "surname"
    t.string   "name"
    t.string   "patronymic"
    t.integer  "type_role"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "salt"
    t.integer  "organization_id"
  end

  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
